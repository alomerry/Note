# 技巧

## Postman

### Environments

![image-20211126145531979](https://gitee.com/alomerry/image-bed/raw/master/note/202111261455035.png)

### Pre-request Script

![image-20211126145802707](https://gitee.com/alomerry/image-bed/raw/master/note/202111261458798.png)



```js
var lastResponseStatus = pm.environment.get("lastResponseStatus");

if (lastResponseStatus != 200) {
    const accountId = pm.environment.get("accountId");
    var data = {
        "account": "",
        "password": "",
        "accountId": accountId
    }
    const openapi_business_domain = pm.environment.get("openapi-business");

    const loginRequest = {
        url: 'https://'+openapi_business_domain+'/v2/login',
        method: 'POST',
        header:'Content-Type:application/json',
        body: {
            mode:'raw',
            raw:JSON.stringify(data)
        }
    };
    pm.sendRequest(loginRequest, function (err, res) {
        if (err) {
            console.log(err);
        } else {
            const jsonData = res.json();
            pm.environment.set('X-Access-Token', jsonData.accessToken);
        }
    });
}
```



### Tests

```js
pm.environment.set('lastResponseStatus', pm.response.code);
```


![image-20211126145833394](https://gitee.com/alomerry/image-bed/raw/master/note/202111261458426.png)



### 请求 API

![image-20211126150100323](https://gitee.com/alomerry/image-bed/raw/master/note/202111261501362.png)