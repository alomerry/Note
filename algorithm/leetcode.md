# [LeetCode 题解](https://github.com/Alomerry/algorithm/tree/master/leet-code)

## Easy <Badge type="tip" text="Easy" vertical="middle" />

- [1 两数之和](https://leetcode-cn.com/problems/two-sum)
- [4 寻找两个正序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays)
- [7 整数反转](https://leetcode-cn.com/problems/reverse-integer)
- [9 回文数](https://leetcode-cn.com/problems/palindrome-number)
- [14 最长公共前缀](https://leetcode-cn.com/problems/longest-common-prefix)
- [20 有效的括号](https://leetcode-cn.com/problems/valid-parentheses/)
- [21 合并两个有序链表](https://leetcode-cn.com/problems/merge-two-sorted-lists/)
- [26 删除有序数组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array)
- [27 移除元素](https://leetcode-cn.com/problems/remove-element/)
- [28 实现 strStr()](https://leetcode-cn.com/problems/implement-strstr)
- [35 搜索插入位置](https://leetcode-cn.com/problems/search-insert-position)
- [53 最大子数组和](https://leetcode-cn.com/problems/maximum-subarray/)
- [58 最后一个单词的长度](https://leetcode-cn.com/problems/length-of-last-word)
- [66 加一](https://leetcode-cn.com/problems/plus-one)
- [67 二进制求和](https://leetcode-cn.com/problems/add-binary)
- [69 Sqrt(x)](https://leetcode-cn.com/problems/sqrtx/)
- [70 爬楼梯](https://leetcode-cn.com/problems/climbing-stairs)
- [83 删除排序链表中的重复元素](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-list)
- [88 合并两个有序数组](https://leetcode-cn.com/problems/merge-sorted-array)
- [1920 基于排列构建数组](https://leetcode-cn.com/problems/build-array-from-permutation)
## Medium <Badge type="warning" text="Medium" vertical="middle" />

- [2 两数相加](https://leetcode-cn.com/problems/add-two-numbers)
- [3 无重复字符的最长子串](https://leetcode-cn.com/problems/longest-substring-without-repeating-characters)
- [5 最长回文子串](https://leetcode-cn.com/problems/longest-palindromic-substring)
- [6 Z 字形变换](https://leetcode-cn.com/problems/zigzag-conversion)
- [8 字符串转换整数 (atoi)](https://leetcode-cn.com/problems/string-to-integer-atoi)
- [11 盛最多水的容器](https://leetcode-cn.com/problems/container-with-most-water)
- [12 整数转罗马数字](https://leetcode-cn.com/problems/integer-to-roman)
- [15 三数之和](https://leetcode-cn.com/problems/3sum)
- [33 搜索旋转排序数组](https://leetcode-cn.com/problems/search-in-rotated-sorted-array)
- [207 课程表](https://leetcode-cn.com/problems/course-schedule)

## Hard <Badge type="danger" text="Hard" vertical="middle" />

- [4 寻找两个正序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays)
- [23 合并K个升序链表](https://leetcode-cn.com/problems/merge-k-sorted-lists)

## All

### [1 两数之和](https://leetcode-cn.com/problems/two-sum) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

:::: code-group
::: code-group-item map
```cpp
vector<int> twoSum(vector<int> &nums, int target)
{
    vector<int> result;
    map<int, int> tmp, count;
    for (int i = 0; i < nums.size(); i++)
    {
        tmp[nums[i]] = i;
        ++count[nums[i]];
    }
    for (int i = 0; i < nums.size(); i++)
    {
        map<int, int>::iterator res = tmp.find(target - nums[i]);
        if (res != tmp.end())
        {
            if (res->first == nums[i] && count[nums[i]] <= 1)
                continue;
            else
            {
                result.push_back(i);
                result.push_back(res->second);
                break;
            }
        }
    }
    return result;
}
```
:::
::: code-group-item pruning
```cpp
vector<int> twoSum(vector<int> &nums, int target)
{
    vector<int> res;
    int tmp = 10e8;
    for (int i = 0; i < nums.size(); i++)
    {
        for (int j = i + 1; j < nums.size(); j++)
        {
            if (abs(nums[j] + nums[i] - target) > tmp)
            {
                break;
            }
            else
            {
                tmp = abs(tmp - target);
            }
            if (nums[j] + nums[i] != target)
            {
                continue;
            }
            else
            {
                res.push_back(i);
                res.push_back(j);
                break;
            }
        }
    }
    return res;
}
```
:::
::::

### [2 两数相加](https://leetcode-cn.com/problems/add-two-numbers) <Badge type="warning" text="Medium" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

violence

```cpp
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};
ListNode *addTwoNumbers(ListNode *l1, ListNode *l2)
{
    int carry = 0;
    ListNode *result = new ListNode(0);
    ListNode *l = result;
    while (l1 != NULL || l2 != NULL)
    {
        int temp = 0;
        if (l1 != NULL)
        {
            temp = l1->val;
            l1 = l1->next;
        }
        if (l2 != NULL)
        {
            temp += l2->val;
            l2 = l2->next;
        }
        temp += carry;
        carry = temp / 10;
        temp %= 10;
        l->next = new ListNode(temp);
        l = l->next;
    }
    if (carry != 0)
    {
        l->next = new ListNode(carry);
        l->next->next = NULL;
    }
    return result->next;
}
```

### [3 无重复字符的最长子串](https://leetcode-cn.com/problems/longest-substring-without-repeating-characters) <Badge type="warning" text="Medium" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

:::: code-group
::: code-group-item main
```cpp
int lengthOfLongestSubstring(string s)
{
    int len = s.size(), max = 0, tmp;
    bool flag[200];
    fill(flag, flag + 200, false);
    queue<char> q;
    for (int i = 0; i < len; i++)
    {
        while (flag[s[i]])
        {
            tmp = q.front();
            q.pop();
            flag[tmp] = false;
        }
        q.push(s[i]);
        flag[s[i]] = true;
        max = q.size() > max ? q.size() : max;
    }
    return max;
}
```
:::
::: code-group-item queue
```cpp
int lengthOfLongestSubstring(string s)
{
    int len = s.size(), max = 0, tmp;
    set<char> flag;
    queue<char> q;
    for (int i = 0; i < s.size(); i++)
    {
        while (flag.find(s[i]) != flag.end())
        {
            tmp = q.front();
            q.pop();
            flag.erase(tmp);
        }
        q.push(s[i]);
        flag.insert(s[i]);
        max = q.size() > max ? q.size() : max;
    }
    return max;
}
```
:::
::::

### [4 寻找两个正序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays) <Badge type="danger" text="Hard" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

:::: code-group
::: code-group-item quickSort
```cpp
void quickSort(vector<int> &nums, int left, int right)
{
    if (left >= right)
        return;
    int i = left + 1, j = right, z, tmp = nums[left];
    while (i != j)
    {
        while (nums[j] >= tmp && i < j)
        {
            j--;
        }
        while (nums[i] <= tmp && i < j)
        {
            i++;
        }
        z = nums[i];
        nums[i] = nums[j];
        nums[j] = z;
    }
    if (tmp > nums[i])
    {
        z = nums[i];
        nums[i] = tmp;
        nums[left] = z;
    }
    quickSort(nums, left, i - 1);
    quickSort(nums, j, right);
}

double findMedianSortedArrays(vector<int> &nums1, vector<int> &nums2)
{
    for (int i = 0; i < nums2.size(); i++)
        nums1.push_back(nums2[i]);
    quickSort(nums1, 0, nums1.size() - 1);
    if (nums1.size() % 2 == 0)
        return 1.0 * (nums1[nums1.size() / 2 - 1] + nums1[nums1.size() / 2]) / 2.0;
    else
        return 1.0 * nums1[nums1.size() / 2];
}
```
:::
::: code-group-item librarySort
```cpp
double findMedianSortedArrays(vector<int> &nums1, vector<int> &nums2)
{
    for (int i = 0; i < nums2.size(); i++)
        nums1.push_back(nums2[i]);
    sort(nums1.begin(), nums1.end());
    if (nums1.size() % 2 == 0)
        return 1.0 * (nums1[nums1.size() / 2 - 1] + nums1[nums1.size() / 2]) / 2.0;
    else
        return 1.0 * nums1[nums1.size() / 2];
}
```
:::
::: code-group-item bubbleSort
```cpp
void bubbleSort(vector<int> &nums)
{
    int temp = 0;
    for (int i = 0; i < nums.size() - 1; i++)
    {
        for (int j = 0; j < nums.size() - 1 - i; j++)
        {
            if (nums[j] > nums[j + 1])
            {
                temp = nums[j];
                nums[j] = nums[j + 1];
                nums[j + 1] = temp;
            }
        }
    }
}
double findMedianSortedArrays(vector<int> &nums1, vector<int> &nums2)
{
    for (int i = 0; i < nums2.size(); i++)
        nums1.push_back(nums2[i]);
    bubbleSort(nums1);
    if (nums1.size() % 2 == 0)
        return 1.0 * (nums1[nums1.size() / 2 - 1] + nums1[nums1.size() / 2]) / 2.0;
    else
        return 1.0 * nums1[nums1.size() / 2];
}
```
:::
::::

### [5 最长回文子串](https://leetcode-cn.com/problems/longest-palindromic-substring) <Badge type="warning" text="Medium" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

:::: code-group
::: code-group-item main
```cpp
string longestPalindrome(string s)
{
    int max = -1, a, b, c, d, size = s.size();
    for (int i = 0; i < size; ++i)
    {
        a = i - 1, b = i + 1;
        while (a >= 0 && b < size)
        {
            if (s[a] == s[b])
            {
                if (max < (b - a + 1))
                {
                    max = b - a + 1;
                    c = a;
                    d = b - a + 1;
                }
            }
            else
                break;
            --a;
            ++b;
        }
    }
    return s.substr(c, d);
}
```
:::
::: code-group-item dp
```cpp
bool dp[1000][1000];

string longestPalindrome(string s)
{
    int max = -1, a = 0, b = 1, j = 0, size = s.size();
    for (int l = 0; l < size; ++l)
    {
        for (int i = 0; i + l < size; ++i)
        {
            j = i + l;
            if (l == 0)
                dp[i][j] = true;
            else if (l == 1)
                dp[i][j] = s[i] == s[j];
            else
                dp[i][j] = s[i] == s[j] && dp[i + 1][j - 1];
            if (dp[i][j] && max < l + 1)
            {
                max = l + 1;
                a = i;
                b = l + 1;
            }
        }
    }
    return s.substr(a, b);
}
```
:::
::::

### [6 Z 字形变换](https://leetcode-cn.com/problems/zigzag-conversion) <Badge type="warning" text="Medium" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

```cpp
string convert(string s, int numRows)
{
    string res = "";
    if (numRows == 1)
        return s;
    vector<char> list[numRows];
    int index = 0, flag = 1;
    for (int i = 0; i < s.size(); ++i)
    {
        list[index].push_back(s[i]);
        index += flag;
        if (index == numRows - 1)
            flag = -1;
        else if (index == 0)
            flag = 1;
    }
    for (int i = 0; i < numRows; ++i)
        for (int j = 0; j < list[i].size(); ++j)
            res.push_back(list[i][j]);
    return res;
}
```

### [7 整数反转](https://leetcode-cn.com/problems/reverse-integer) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

```cpp
int reverse(int x)
{
    if (x > 2147483647 || x < -2147483648)
        return 0;
    bool isPositive = x >= 0;
    if (!isPositive)
    {
        if (x < -2147483647 || x > 2147483648)
            return 0;
        x = abs(x);
    }
    long res = 0;
    while (x > 0)
    {
        res = res * 10 + x % 10;
        if (res > 2147483647 || res < -2147483648)
            return 0;
        x /= 10;
    }
    return isPositive ? res : -1 * res;
}
```

### [8 字符串转换整数 (atoi)](https://leetcode-cn.com/problems/string-to-integer-atoi) <Badge type="warning" text="Medium" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

```cpp
bool isPositive = true;

string clearSpace(string str)
{
    int size = str.size(), i;
    bool isFirstCharacter = true, isPositive = true;
    for (i = 0; i < size; ++i)
        if (str[i] == ' ')
            continue;
        else
            break;
    return str.substr(i, size - i);
}

bool setIsPositive(string &str)
{
    if (str[0] == '+')
    {
        isPositive = true;
        str = str.substr(1, str.size() - 1);
        return true;
    }
    if (str[0] == '-')
    {
        isPositive = false;
        str = str.substr(1, str.size() - 1);
        return true;
    }
    if (str[0] >= '0' && str[0] <= '9')
        return true;
    return false;
}

int myAtoi(string str)
{
    str = clearSpace(str);
    long res = 0;
    if (!setIsPositive(str))
    {
        return 0;
    }
    for (int i = 0; i < str.size(); ++i)
    {
        if (str[i] <= '9' && str[i] >= '0')
            res = res * 10 + str[i] - '0';
        else
            break;
        if (res > 2147483648 && !isPositive)
            return -2147483648;
        else if (res > 2147483647 && isPositive)
            return 2147483647;
    }
    return (isPositive ? 1 : -1) * res;
}
```

### [9 回文数](https://leetcode-cn.com/problems/palindrome-number) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

```cpp
string convertToStr(int x)
{
    string str = "";
    while (x > 0)
    {
        str += ('0' + x % 10);
        x /= 10;
    }
    return str;
}

bool isPalindrome(int x)
{
    if (x < 0)
        return false;
    if (x == 0)
        return true;

    string str = convertToStr(x);
    for (int i = 0, j = str.size() - 1; i <= j; i++, j--)
        if (str[i] != str[j])
            return false;
    return true;
}
```

### [11 盛最多水的容器](https://leetcode-cn.com/problems/container-with-most-water) <Badge type="warning" text="Medium" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

dp

```cpp
vector<int> tmp1, tmp2;
int maxi = 0;
void dp(vector<int> &height, int left, int right)
{
    if (left >= right)
        return;
    if (height[left] < height[right])
    {
        int now = height[left] * abs(left - right);
        maxi = maxi > now ? maxi : now;
        dp(height, left + 1, right);
    }
    else
    {
        int now = height[right] * abs(left - right);
        maxi = maxi > now ? maxi : now;
        dp(height, left, right - 1);
    }
}

int maxArea(vector<int> &height)
{
    dp(height, 0, height.size() - 1);
    return maxi;
}
```

### [12 整数转罗马数字](https://leetcode-cn.com/problems/integer-to-roman) <Badge type="warning" text="Medium" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

```cpp
vector<int> key = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
map<int, int> charNumber;
map<int, string> charMapper = {
    {1000, "M"},
    {900, "CM"},
    {500, "D"},
    {400, "CD"},
    {100, "C"},
    {90, "XC"},
    {50, "L"},
    {40, "XL"},
    {10, "X"},
    {9, "IX"},
    {5, "V"},
    {4, "IV"},
    {1, "I"},
};

void format(int now, int index)
{
    if (index >= key.size())
        return;
    int num = now / key[index];
    if (num > 0)
    {
        charNumber[key[index]] += num;
        now -= num * key[index];
    }
    format(now, index + 1);
}

string intToRoman(int num)
{
    format(num, 0);
    string res = "";
    for (int i = 0; i < key.size(); ++i)
    {
        while (charNumber[key[i]] > 0)
        {
            res += charMapper[key[i]];
            --charNumber[key[i]];
        }
    }
    return res;
}
```

### [14 最长公共前缀](https://leetcode-cn.com/problems/longest-common-prefix) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

```cpp
string getMaxPrefix(string a, string b)
{
    int i;
    for (i = 0; i < a.size() && i < b.size(); ++i)
        if (a[i] != b[i])
            break;
    return a.substr(0, i);
}

string dp(vector<string> strs, int left, int right)
{
    if (left >= right)
        return strs[left];
    int middle = left + (right - left) / 2;
    string leftStr = dp(strs, left, middle);
    string rightStr = dp(strs, middle + 1, right);
    return getMaxPrefix(leftStr, rightStr);
}

string longestCommonPrefix(vector<string> &strs)
{
    if (strs.size() > 0)
        return dp(strs, 0, strs.size() - 1);
    return "";
}
```

### [15 三数之和](https://leetcode-cn.com/problems/3sum) <Badge type="warning" text="Medium" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

```cpp
bool check(vector<int> sets, int left, int right, int needValue)
{
    if (mapper[needValue] <= 0)
        return false;
    for (int i = left + 1; i < right && i < sets.size(); ++i)
        if (sets[i] == needValue)
            return true;
    return false;
}

void dp(vector<int> sets, int left, int right)
{
    if (left >= right || left + 1 == right)
        return;
    int need = 0 - sets[left] - sets[right];
    if (check(sets, left, right, need))
    {
        vector<int> tmp = {sets[left], sets[right], need};
        res.push_back(tmp);
    }
    int lefter = left, righter = right;
    while (lefter + 1 < righter && sets[lefter] == sets[lefter + 1])
        ++lefter;
    while (righter - 1 > righter && sets[righter] == sets[righter - 1])
        --righter;
    dp(sets, lefter + 1, righter);
    dp(sets, lefter, righter - 1);
}

vector<vector<int> > threeSum(vector<int> &nums)
{
    for (int i = 0; i < nums.size(); ++i)
        ++mapper[nums[i]];
    sort(nums.begin(), nums.end());
    dp(nums, 0, nums.size() - 1);
    return res;
}
```

### [20 有效的括号](https://leetcode-cn.com/problems/valid-parentheses/) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

给定一个只包括`'('`，`')'`，`'{'`，`'}'`，`'['`，`']'` 的字符串 s ，判断字符串是否有效。

有效字符串需满足：

- 左括号必须用相同类型的右括号闭合。
- 左括号必须以正确的顺序闭合。

示例 1：

```
输入：s = "()"
输出：true
```

示例 2：

```
输入：s = "()[]{}"
输出：true
```

示例 3：

```
输入：s = "(]"
输出：false
```

示例 4：

```
输入：s = "([)]"
输出：false
```

示例 5：

```
输入：s = "{[]}"
输出：true
```

提示：

- `1 <= s.length <= 104`
- `s` 仅由括号 `'()[]{}'` 组成

:::

#### Ideas

xxx

#### Code

![20](https://gitee.com/alomerry/image-bed/raw/master/note/202111290519718.png)

stack

```cpp
bool isValid(string s)
{
    set<char> left;
    string ls = "({[", rs = ")}]";
    map<char, char> m;

    for (int i = 0; i < ls.size(); i++)
    {
        left.insert(ls[i]);
        m[ls[i]] = rs[i];
    }

    stack<char> st;
    set<char>::iterator it;
    for (int i = 0; i < s.size(); i++)
    {
        it = left.find(s[i]);
        if (it != left.end())
        {
            st.push(s[i]);
            continue;
        }
        if (st.empty())
            return false;
        char top = st.top();
        st.pop();
        if (m[top] != s[i])
            return false;
    }
    return st.empty();
}
/**
 思路：`( [ {` 需要和 `) ] }` 对应，`) ] }` 和最近的 `( [ {` 对应，则需要前进后出的栈辅助，如果是 `( [ {`，则放入栈中，否则获取栈顶元素，判断是否和当前的 `) ] }` 对应。
 */
```

### [21 合并两个有序链表](https://leetcode-cn.com/problems/merge-two-sorted-lists/) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。

![图片](https://assets.leetcode.com/uploads/2020/10/03/merge_ex1.jpg)

示例 1：

```
输入：l1 = [1,2,4], l2 = [1,3,4]
输出：[1,1,2,3,4,4]
```

示例 2：

```
输入：l1 = [], l2 = []
输出：[]
```

示例 3：

```
输入：l1 = [], l2 = [0]
输出：[0]
```

提示：

- 两个链表的节点数目范围是 `[0, 50]`
- `-100 <= Node.val <= 100`
- `l1` 和  `12` 均按 非递减顺序 排列

:::

#### Ideas

xxx

#### Code

![21-1](https://gitee.com/alomerry/image-bed/raw/master/note/202111290514085.png)

![21-2](https://gitee.com/alomerry/image-bed/raw/master/note/202111290514016.png)

:::: code-group
::: code-group-item dp
```cpp
struct ListNode
{
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

ListNode *mergeTwoLists(ListNode *l1, ListNode *l2)
{
    if (l1 == NULL)
        return l2;
    if (l2 == NULL)
        return l1;
    if (l1->val <= l2->val)
    {
        l1->next = mergeTwoLists(l1->next, l2);
        return l1;
    }
    else
    {
        l2->next = mergeTwoLists(l1, l2->next);
        return l2;
    }
}
```
:::
::: code-group-item main
```cpp
ListNode *mergeTwoLists(ListNode *l1, ListNode *l2)
{
    if (l1 != NULL)
        return l1;
    if (l2 != NULL)
        return l2;
    ListNode *small = l1, *big = l2, *res, *now;
    if (l1->val > l2->val)
    {
        small = l2;
        big = l1;
    }
    res = small;
    now = res;
    small = small->next;
    while (small != NULL && big != NULL)
    {
        if (small->val <= big->val)
        {
            now->next = small;
            now = now->next;
            small = small->next;
        }
        else
        {
            now->next = big;
            now = now->next;
            big = big->next;
        }
    }

    if (small != NULL)
        now->next = small;
    if (big != NULL)
        now->next = big;
    return res;
}

/**
 思路：使用游标始终定位到最小的节点
 */

```
:::
::::

### [23 合并K个升序链表](https://leetcode-cn.com/problems/merge-k-sorted-lists) <Badge type="danger" text="Hard" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

```cpp
struct ListNode
{
    int val;
    ListNode *next;

    ListNode(int x) : val(x), next(NULL) {}
};

set<int> miniSet;
map<int, queue<int> > valueIndexMapper;

ListNode *mergeKLists(vector<ListNode *> &lists)
{
    ListNode *head = new ListNode(0), *tmp = head;
    for (int i = 0; i < lists.size(); ++i)
    {
        if (lists[i] != nullptr)
        {
            valueIndexMapper[lists[i]->val].push(i);
            miniSet.insert(lists[i]->val);
        }
    }
    while (miniSet.size() > 0)
    {
        int miniNow = *miniSet.begin();
        queue<int> q = valueIndexMapper[miniNow];
        while (!q.empty())
        {
            int index = q.front();
            ListNode *item = lists[index];
            lists[index] = lists[index]->next;
            tmp->next = item;
            tmp = tmp->next;
            while (item->next != NULL && item->next->val == miniNow)
            {
                item = lists[index];
                lists[index] = lists[index]->next;
                tmp->next = item;
                tmp = tmp->next;
            }
            if (item->next != NULL)
            {
                miniSet.insert(item->next->val);
                valueIndexMapper[item->next->val].push(q.front());
            }
            q.pop();
        }
        miniSet.erase(miniNow);
    }
    return head->next;
}
```

### [26 删除有序数组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

```cpp
int removeDuplicates(vector<int> &nums)
{
    if (nums.size() <= 1)
        return nums.size();

    int left = 0, right = 1;
    for (; right < nums.size(); right++)
        if (nums[left] != nums[right])
            nums[++left] = nums[right];
    return left + 1;
}
```

### [27 移除元素](https://leetcode-cn.com/problems/remove-element/) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

给你一个数组 `nums` 和一个值 `val`，你需要 原地 移除所有数值等于 `val` 的元素，并返回移除后数组的新长度。

不要使用额外的数组空间，你必须仅使用 `O(1)` 额外空间并 **原地** 修改输入数组。

元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。


说明:

为什么返回数值是整数，但输出的答案是数组呢?

请注意，输入数组是以**「引用」**方式传递的，这意味着在函数里修改输入数组对于调用者是可见的。

你可以想象内部操作如下：

```tex
// nums 是以“引用”方式传递的。也就是说，不对实参作任何拷贝
int len = removeElement(nums, val);

// 在函数里修改输入数组对于调用者是可见的。
// 根据你的函数返回的长度, 它会打印出数组中 该长度范围内 的所有元素。
for (int i = 0; i < len; i++) {
    print(nums[i]);
}
```

示例 1：

```tex
输入：nums = [3,2,2,3], val = 3
输出：2, nums = [2,2]
解释：函数应该返回新的长度 2, 并且 nums 中的前两个元素均为 2。你不需要考虑数组中超出新长度后面的元素。例如，函数返回的新长度为 2 ，而 nums = [2,2,3,3] 或 nums = [2,2,0,0]，也会被视作正确答案。
```

示例 2：

```tex
输入：nums = [0,1,2,2,3,0,4,2], val = 2
输出：5, nums = [0,1,4,0,3]
解释：函数应该返回新的长度 5, 并且 nums 中的前五个元素为 0, 1, 3, 0, 4。注意这五个元素可为任意顺序。你不需要考虑数组中超出新长度后面的元素。
```

提示：

- `0 <= nums.length <= 100`
- `0 <= nums[i] <= 50`
- `0 <= val <= 100`

:::

#### Ideas

Todo

#### Code

![27](https://gitee.com/alomerry/image-bed/raw/master/note/202112060405326.png)

```cpp
int removeElement(vector<int> &nums, int val)
{
    if (nums.size() == 0)
        return 0;
    int begin = 0, end = nums.size() - 1;
    while (begin != end)
    {
        if (nums[begin] != val)
        {
            begin++;
            continue;
        }
        if (nums[end] == val)
        {
            end--;
            continue;
        }

        swap(nums[begin], nums[end]);
    }

    if (nums[begin] == val)
        return begin;

    return begin + 1;
}
```

### [28 实现 strStr()](https://leetcode-cn.com/problems/implement-strstr) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

kmp

```cpp
vector<int> calculateNext(string s){
    int i = 0, j = -1;
    vector<int> next(s.size()+1, -1);
    next[i] = j;
    while (true){
        if ( j == -1 | s[i] == s[j] ) {
            ++i;
            ++j;
            next[i] = j;
        }
    }
}
```

### [33 搜索旋转排序数组](https://leetcode-cn.com/problems/search-in-rotated-sorted-array) <Badge type="warning" text="Medium" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

binarySearch

```cpp
void binarySearch(vector<int> &nums, int left, int right, int target)
{
    if (left >= right)
    {
        if (target == nums[left])
            result = left;
        return;
    }
    int middle = left + (right - left) / 2;
    binarySearch(nums, left, middle, target);
    binarySearch(nums, middle + 1, right, target);
}

int search(vector<int> &nums, int target)
{
    if (nums.size() == 0)
        return -1;
    binarySearch(nums, 0, nums.size() - 1, target);
    return result;
}
```

### [35 搜索插入位置](https://leetcode-cn.com/problems/search-insert-position) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

```cpp
int binarySearch(vector<int> nums, int left, int right, int target)
{
    if (target < nums[left])
        return left;
    if (target > nums[right])
        return right + 1;
    if (left >= right)
    {
        if (target <= nums[left])
            return left;
        else
            return left + 1;
    }
    int middle = left + (right - left) / 2;
    if (target == nums[middle])
        return middle;
    else if (target < nums[middle])
        return binarySearch(nums, left, middle, target);
    else
        return binarySearch(nums, middle + 1, right, target);
}

int searchInsert(vector<int> &nums, int target)
{
    cout << binarySearch(nums, 0, nums.size() - 1, target) << endl;
}
```

### [53 最大子数组和](https://leetcode-cn.com/problems/maximum-subarray/) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

给你一个整数数组 nums ，请你找出一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。

子数组 是数组中的一个连续部分。

示例 1

```tex
输入：nums = [-2,1,-3,4,-1,2,1,-5,4]
输出：6
解释：连续子数组 [4,-1,2,1] 的和最大，为 6 。
```

示例 2

```tex
输入：nums = [1]
输出：1
```

示例 3

```tex
输入：nums = [5,4,-1,7,8]
输出：23
```

提示：

- `1 <= nums.length <= 105`
- `104 <= nums[i] <= 104`

进阶：如果你已经实现复杂度为 O(n) 的解法，尝试使用更为精妙的 分治法 求解。

:::

#### Idea

some ideas...

#### Code

![53](https://gitee.com/alomerry/image-bed/raw/master/note/202112060618371.png)

dp

```cpp
#define INF 0x3f3f3f3f

int maxSubArray(vector<int> &nums)
{
    int mini = nums[nums.size() - 1];
    if (nums.size() <= 1)
        return nums[0];
    for (int i = nums.size() - 2; i >= 0; i--)
    {
        if (nums[i + 1] > 0)
            nums[i] += nums[i + 1];
        if (mini < nums[i])
            mini = nums[i];
    }
    return mini;
}
```

### [58 最后一个单词的长度](https://leetcode-cn.com/problems/length-of-last-word) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

![58](https://gitee.com/alomerry/image-bed/raw/master/note/202112060618026.png)

```cpp
int lengthOfLastWord(string s)
{
    int len = 0;
    for (int i = 0; i < s.size(); i++)
    {
        if (s[i] == ' ')
        {
            len = -abs(len);
            continue;
        }
        if (len < 0)
            len = 0;
        len++;
    }
    return abs(len);
}
```

### [66 加一](https://leetcode-cn.com/problems/plus-one) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

![66](https://gitee.com/alomerry/image-bed/raw/master/note/202112060618341.png)

```cpp
vector<int> plusOne(vector<int> &digits)
{
    stack<int> s;
    int needAdd = 1;
    for (int i = digits.size() - 1; i >= 0; i--)
    {
        if (needAdd > 0)
        {
            if (digits[i] + needAdd > 9)
            {
                s.push(digits[i] + needAdd - 10);
                needAdd = 1;
                continue;
            }
        }
        s.push(digits[i] + needAdd);
        needAdd = 0;
    }
    if (needAdd > 0)
        s.push(needAdd);

    vector<int> result;
    while (!s.empty())
    {
        int i = s.top();
        s.pop();
        result.push_back(i);
    }
    return result;
}
```

### [67 二进制求和](https://leetcode-cn.com/problems/add-binary) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

![67](https://gitee.com/alomerry/image-bed/raw/master/note/202112060619678.png)

```cpp
string addBinary(string a, string b)
{
    stack<char> s;
    int i = a.size() - 1, j = b.size() - 1, inc = 0;
    while (i >= 0 && j >= 0)
    {
        inc += a[i] - '0' + b[j] - '0';
        s.push(inc % 2 + '0');
        inc /= 2;
        i--, j--;
    }

    while (i >= 0)
    {
        inc += a[i] - '0';
        s.push(inc % 2 + '0');
        inc /= 2;
        i--;
    }

    while (j >= 0)
    {
        inc += b[j] - '0';
        s.push(inc % 2 + '0');
        inc /= 2;
        j--;
    }

    if (inc != 0)
    {
        s.push(inc + '0');
    }

    string result = "";
    while (!s.empty())
    {
        result += s.top();
        s.pop();
    }
    return result;
}
```


### [69 Sqrt(x)](https://leetcode-cn.com/problems/sqrtx/) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

给你一个非负整数 x ，计算并返回 x 的 算术平方根 。

由于返回类型是整数，结果只保留 整数部分 ，小数部分将被 舍去 。

注意：不允许使用任何内置指数函数和算符，例如 pow(x, 0.5) 或者 x ** 0.5 。

示例 1：

```
输入：x = 4
输出：2
```

示例 2：

```
输入：x = 8
输出：2
```

解释：8 的算术平方根是 2.82842..., 由于返回类型是整数，小数部分将被舍去。

:::

#### Ideas

xxx

#### Code

![69](https://gitee.com/alomerry/image-bed/raw/master/note/202110091523247.png)

```cpp
int binarySearch(int x, int left, int right)
{
    if (left == right)
        return left;
    int half = left + (right - left) / 2;
    if (half <= x / half && (half + 1) > x / (half + 1))
        return half;
    if (x / half <= half)
        return binarySearch(x, left, half);
    else
        return binarySearch(x, half + 1, right);
}

int mySqrt(int x)
{
    if (x <= 1)
        return x;
    return binarySearch(x, 1, x);
}
```

### [70 爬楼梯](https://leetcode-cn.com/problems/climbing-stairs) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

![image-20211206062025244](https://gitee.com/alomerry/image-bed/raw/master/note/202112060620325.png)

```cpp
int climbStairs(int n)
{
    if (n <= 2)
        return n;
    int p = 1, q = 2, j = 0;
    for (int i = 3; i <= n; i++)
    {
        j = p + q;
        p = q;
        q = j;
    }
    return j;
}
```

### [83 删除排序链表中的重复元素](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-list) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

![image-20211206062054605](https://gitee.com/alomerry/image-bed/raw/master/note/202112060620687.png)

```cpp
struct ListNode
{
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

ListNode *deleteDuplicates(ListNode *head)
{
    ListNode *now = head, *next = head;
    while (next != NULL)
    {
        if (next->val != now->val)
        {
            now->next = next;
            now = next;
        }
        next = next->next;
    }
    if (now != NULL && now->next != NULL && now->next->val == now->val)
        now->next = NULL;
    return head;
}
```

### [88 合并两个有序数组](https://leetcode-cn.com/problems/merge-sorted-array) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

```cpp
void merge(vector<int> &nums1, int m, vector<int> &nums2, int n)
{
    int index = (m--) + (n--) - 1;
    while (m >= 0 && n >= 0)
    {
        if (nums1[m] >= nums2[n])
            nums1[index--] = nums1[m--];
        else
            nums1[index--] = nums2[n--];
    }

    while (m >= 0)
        nums1[index--] = nums1[m--];

    while (n >= 0)
        nums1[index--] = nums2[n--];
}
```

### [207 课程表](https://leetcode-cn.com/problems/course-schedule) <Badge type="warning" text="Medium" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

![88](https://gitee.com/alomerry/image-bed/raw/master/note/202112060621648.png)

![88-2](https://gitee.com/alomerry/image-bed/raw/master/note/202112060622428.png)


```cpp
bool canFinish(int numCourses, vector<vector<int> > &prerequisites)
    {
        int ind[numCourses] = {0}, i, u, l, item;
        vector<int> gra[numCourses];
        l = prerequisites.size();
        for (i = 0; i < l; i++)
        {
            gra[prerequisites[i][0]].push_back(prerequisites[i][1]);
            ++ind[prerequisites[i][1]];
        }
        queue<int> q;
        for (i = 0; i < numCourses; i++)
            if (ind[i] == 0)
                q.push(i);
        while (!q.empty())
        {
            u = q.front();
            q.pop();
            l = gra[u].size();
            for (i = 0; i < gra[u].size(); i++)
            {
                item = gra[u][i];
                --ind[item];
                if (ind[item] == 0)
                    q.push(item);
            }
            --numCourses;
        }
        return numCourses == 0;
    }
```

### [1920 基于排列构建数组](https://leetcode-cn.com/problems/build-array-from-permutation) <Badge type="tip" text="Easy" vertical="middle" />

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

```cpp
vector<int> buildArray(vector<int> &nums)
{
    vector<int> result = vector<int>(nums.size(), 0);
    for (int i = 0; i < nums.size(); i++)
        result[i] = nums[nums[i]];
    return result;
}
```


### [Test ]()

#### Description

::: details Description

示例 1

```tex
some description...
```

:::

#### Ideas

some ideas...

#### Code

:::: code-group
::: code-group-item a
```cpp
// some code...
```
:::
::: code-group-item b
```cpp
// some code...
```
:::
::::
